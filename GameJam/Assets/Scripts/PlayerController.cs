﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static float trampolineForce =3;
    public static float jumpForce =2;
    public float vel;
    private Rigidbody rb;

    public Vector3 movement;
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        
    }

    // Update is called once per frame
    void Update()
    {
        movement.x = Input.GetAxis("Horizontal");
        rb.MovePosition(rb.position + movement * -vel * Time.fixedDeltaTime);
        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    rb.velocity = new Vector3(-vel, 0, 0);
        //  
        //}
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    rb.velocity = new Vector3(vel, 0, 0);
        //   
        //}
        //if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A))
        //{
        //    rb.velocity = new Vector3(0, 0, 0);
        //}
    }


    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            Rigidbody rigidbody = collision.gameObject.GetComponent<Rigidbody>();

            //ARREGLAR que no sea random, si no que rebote mas cada 3 veces que rebota
            //rigidbody.velocity = new Vector3(0, Random.Range(2, 5));
            //Debug.Log(rigidbody.velocity);
            rigidbody.AddForce(new Vector3(0, jumpForce * trampolineForce, 0), ForceMode.Impulse);
        }

    }
}
