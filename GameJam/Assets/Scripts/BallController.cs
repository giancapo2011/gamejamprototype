﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{


    public Transform disparador;
    public Rigidbody balaImpulso;
    public float speed;
    

    Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(speed, 0, 0);
    }
    private void Update()
    {


    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            rb.AddForce(new Vector3(-speed, 0, 0), ForceMode.Impulse);
        }

        if (collision.gameObject.CompareTag("Wall01"))
        {
            rb.AddForce(new Vector3(speed, 0, 0), ForceMode.Impulse);
        }

    }

}