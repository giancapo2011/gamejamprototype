﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            Rigidbody rigidbody = collision.gameObject.GetComponent<Rigidbody>();

            //ARREGLAR que no sea random, si no que rebote mas cada 3 veces que rebota
            //rigidbody.velocity = new Vector3(0, Random.Range(2, 5));
            //Debug.Log(rigidbody.velocity);
            rigidbody.AddForce(new Vector3(0, 0, PlayerController.jumpForce * PlayerController.trampolineForce), ForceMode.Impulse);
        }

    }
}
