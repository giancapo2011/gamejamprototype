﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    float trampolineForceReference = 6f;
    float trampolineForce;
    public static bool controlledEjection;
    public float speed;
    Rigidbody rb;
    public Transform playerTransform;
    Vector3 offset;
    private void Start()
    {
        trampolineForce = trampolineForceReference;
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(speed, 0, 0);
        offset = new Vector3(0, 0.4f, 0);
    }
    private void Update()
    {
        if (controlledEjection)
        {
            transform.SetParent(playerTransform);
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().isKinematic = true;  
        }
        if (!controlledEjection)
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionZ;
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().isKinematic = false;
            transform.SetParent(null);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController.isTouching = false;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController.isTouching = true;
            if (!Controller_LaunchBar.isPlayerUsingCharge)
            {
                CalculateCoords(transform.position.x, collision.gameObject.transform.position.x, collision.collider.bounds.size.x);
            }
        }
        if (collision.gameObject.CompareTag("Wall"))
        {
            Vector3 dir = collision.contacts[0].point - transform.position;
            dir = -dir.normalized;
            rb.AddForce(dir * 4, ForceMode.Impulse);
        }
    }
    void CalculateCoords(float ballPos, float trampolinePos, float trampolineWidth)
    {
        float resolveValue = (ballPos - trampolinePos) / trampolineWidth;
        Debug.Log(resolveValue);
        giveForce(resolveValue);
        return;
    }

    void giveForce(float finalResult)
    {
        if (Controller_LaunchBar.timer > 0.5f)
        {
            trampolineForce *= Controller_LaunchBar.timer;
            Debug.Log(trampolineForce);
            Controller_LaunchBar.timer = 0;
        }
        if (finalResult > 0.38f)
        {
            Debug.Log("Etapa1");

            rb.AddForce(new Vector3(trampolineForce, trampolineForce, 0), ForceMode.Impulse);
        }
        if (finalResult < -0.38f)
        {
            Debug.Log("Etapa2");

            rb.AddForce(new Vector3(-trampolineForce, trampolineForce, 0), ForceMode.Impulse);
        }
        if (finalResult > -0.20f && finalResult < 0.20f)
        {
            Debug.Log("Etapa3");

            rb.AddForce(new Vector3(0, trampolineForce, 0), ForceMode.Impulse);
        }
        if (finalResult < -0.20f && finalResult > -0.38f)
        {
            Debug.Log("Etapa4");

            rb.AddForce(new Vector3(-trampolineForce / 4, trampolineForce, 0), ForceMode.Impulse);
        }
        if (finalResult > 0.20f && finalResult < 0.38f)
        {
            Debug.Log("Etapa5");

            rb.AddForce(new Vector3(trampolineForce / 4, trampolineForce, 0), ForceMode.Impulse);
        }

        trampolineForce = trampolineForceReference;
        Debug.Log(rb.velocity);
    }
}