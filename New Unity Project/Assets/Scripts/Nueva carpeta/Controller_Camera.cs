﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    public List<Transform> targetsPlayer;
    public Vector3 offset;
    float minZoom = 4f;
    float maxZoom = 2f;
    float zoomLimit = 5f;
    private Vector3 velocity;
    Camera cam;
    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void LateUpdate()
    {
        if (targetsPlayer.Count == 0)
            return;
        MoveCamera();
        ZoomCamera();
    }
    private void ZoomCamera()
    {
        float newZoom = Mathf.Lerp(maxZoom, minZoom, GetDistance() / zoomLimit);
        cam.orthographicSize = newZoom;
    }
    private void MoveCamera()
    {
        Vector3 cPoint = GetCenterPoint();
        Vector3 positionToLook = cPoint + offset;
        //transform.position = Vector3.SmoothDamp(transform.position, positionToLook, ref velocity, .0f);
    }
    Vector3 GetCenterPoint()
    {
        if (targetsPlayer.Count == 1)
        {
            return targetsPlayer[0].position;
        }
        var bounds = new Bounds(targetsPlayer[0].position, Vector3.zero);
        for (int i = 0; i < targetsPlayer.Count; i++)
        {
            bounds.Encapsulate(targetsPlayer[i].position);
        }
        return bounds.center;
    }
    float GetDistance()
    {
        var bounds = new Bounds(targetsPlayer[0].position, Vector3.zero);
        for (int i = 0; i < targetsPlayer.Count; i++)
        {
            bounds.Encapsulate(targetsPlayer[i].position);
        }

        return bounds.size.x;
    }
    /*
    GameObject player;
    

    private void Start()
    {
        player = GameObject.Find("Player");
        offset = transform.position - player.transform.position;
    }

    private void Update()
    {
       this.transform.position = player.transform.position + offset;
    }*/
}
