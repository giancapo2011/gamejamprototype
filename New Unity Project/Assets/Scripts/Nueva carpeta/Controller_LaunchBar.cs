﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_LaunchBar : MonoBehaviour
{
    public Image referenceBar;
    public Transform chargeBar;
    public Transform playerFollower;
    public static float timer;
    public Rigidbody ball;
    bool checkOnlyOnce = true;
    bool touchedEndBar = false;
    public static bool isPlayerUsingCharge;
    private void Start()
    {
        referenceBar.gameObject.SetActive(false);
    }
    public void FixedUpdate()
    {
        Debug.Log(timer);
        if (isPlayerUsingCharge)
        {
            StartCharging();
            if (touchedEndBar is false && timer < 2)
                timer += Time.deltaTime;
            else if (touchedEndBar is true && timer > 0)
                timer -= Time.deltaTime;
            if (timer < 0)
                timer = 0;
            checkOnlyOnce = false;
        }
        if (!isPlayerUsingCharge && !checkOnlyOnce)
        {
            ResetBar();
            checkOnlyOnce = true;
        }
    }
    public void StartCharging()
    {
        referenceBar.gameObject.SetActive(true);
        if (touchedEndBar == false)
        {
            chargeBar.gameObject.transform.localScale += new Vector3(0, -0.5f, 0) * Time.deltaTime;
        }
        if (chargeBar.gameObject.transform.localScale.y > 0.99f)
        {
            touchedEndBar = false;
        }
        if (chargeBar.gameObject.transform.localScale.y < 0.01f || touchedEndBar is true)
        {
            touchedEndBar = true;
            chargeBar.gameObject.transform.localScale += new Vector3(0, 0.5f, 0) * Time.deltaTime;
        }

    }
    void ResetBar()
    {
        chargeBar.gameObject.transform.localScale = referenceBar.gameObject.transform.localScale;
        ball.AddForce(new Vector3(0, 0.8f, 0), ForceMode.Impulse);
        referenceBar.gameObject.SetActive(false);
    }
}
