﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        bool isInInput = Input.GetKey(KeyCode.S);
        if (!isInInput)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                //reanuda el tiempo
                Time.timeScale = 1;
                //carga la escena desde 0 para que el juego vuelva a comenzar
                SceneManager.LoadScene(0);
            }
        }
    }
}
