﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static float trampolineForce =3;
    public static float jumpForce =2;
    public static bool isTouching = false;
    public float vel;
    private Rigidbody rb;

    public Vector3 movement;

   
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        movement.x = Input.GetAxis("Horizontal");
        rb.MovePosition(rb.position + movement * -vel * Time.fixedDeltaTime);
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            rb.velocity = new Vector3(0, 0, 0);
        }
        if (Input.GetKey(KeyCode.S) && isTouching)
        {
            BallController.controlledEjection = true;
            Controller_LaunchBar.isPlayerUsingCharge = true;   
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            BallController.controlledEjection = false;
            Controller_LaunchBar.isPlayerUsingCharge = false;
        }
        #region
        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    rb.velocity = new Vector3(-vel, 0, 0);
        //  
        //}
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    rb.velocity = new Vector3(vel, 0, 0);
        //   
        //}
        #endregion
    }
}
